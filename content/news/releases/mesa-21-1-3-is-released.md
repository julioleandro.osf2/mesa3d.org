---
title:    "Mesa 21.1.3 is released"
date:     2021-06-18 17:57:37
category: releases
tags:     []
---
[Mesa 21.1.3](https://docs.mesa3d.org/relnotes/21.1.3.html) is released. This is a bug fix release.
