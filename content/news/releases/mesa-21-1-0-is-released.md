---
title:    "Mesa 21.1.0 is released"
date:     2021-05-05 22:26:35
category: releases
tags:     []
---
[Mesa 21.1.0](https://docs.mesa3d.org/relnotes/21.1.0.html) is released. This is a new development release. See the release notes for more information about this release.
