---
title:    "Mesa 20.2.2 is released"
date:     2020-11-06 20:53:34
category: releases
tags:     []
---
[Mesa 20.2.2](https://docs.mesa3d.org/relnotes/20.2.2.html) is released. This is a bug fix release.
