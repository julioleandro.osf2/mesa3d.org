---
title:    "Mesa 21.2.5 is released"
date:     2021-10-28 15:45:57
category: releases
tags:     []
---
[Mesa 21.2.5](https://docs.mesa3d.org/relnotes/21.2.5.html) is released. This is a bug fix release.
