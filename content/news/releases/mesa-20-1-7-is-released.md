---
title:    "Mesa 20.1.7 is released"
date:     2020-09-02 22:37:43
category: releases
tags:     []
---
[Mesa 20.1.7](https://docs.mesa3d.org/relnotes/20.1.7.html) is released. This is a bug fix release.
