---
title:    "Mesa 21.1.7 is released"
date:     2021-08-11 22:14:09
category: releases
tags:     []
---
[Mesa 21.1.7](https://docs.mesa3d.org/relnotes/21.1.7.html) is released. This is a bug fix release.
